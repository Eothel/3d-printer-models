# PS3 controller

In order to have a PS3 controller talk to the ESP32, you have to pair them so that the PS3 controller sends data to the ESP32 and not any other device (e.g. like your PS3 console).

Pairing is done by storing the MAC address of the device to communicate with in the PS3 controller. When pairing with the PS3 console itself, this is done by first connecting the two with a USB cable.
When using the ESP32 this is not possible (speculation?), and the pairing has to be done using [a tool](tools/SixaxisPariToolSetup-0.3.1.exe) on the PC.

1. Figure out what the Bluetooth MAC address is of your ESP32.
2. Run the tool and save the ESP32 MAC address in the controller.