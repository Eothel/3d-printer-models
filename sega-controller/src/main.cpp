#include <Arduino.h>
#include <Adafruit_SSD1306.h>
#include <SPI.h>
#include <Wire.h>
#include <SD.h>
#include <Ps3Controller.h>

const uint8_t DOWN_PIN = 13;
const uint8_t LEFT_PIN = 12;
const uint8_t UP_PIN = 14;
const uint8_t RIGHT_PIN = 27;
const uint8_t ONE_PIN = 26;
const uint8_t TWO_PIN = 25;

const uint8_t LINE_HEIGHT = 8;

const uint8_t OLED_DISPLAY_I2C_ADDRESS = 0x3C;
const uint8_t SD_CARD_SPI_CHIP_SELECT_PIN = 5;

const float AUTO_TRIGGER_FREQUENCY_INCREMENT = 0.5f;

Adafruit_SSD1306 display(128, 64, &Wire, -1);

boolean isRecording = false;
boolean isAutotriggerOne = false;
boolean isAutotriggerTwo = false;
File file;
String filename;
unsigned long startRecordingTime = 0;


float autoTriggerFrequency = 1.0f; // 500ms on and 500ms off

void halt(String message) {
  Serial.println(String("HALT: ") + message);
  while(true);
}

void logInteraction(bool up, bool down, String buttonName) {
    unsigned long time = millis() - startRecordingTime;
    if(up) {
      file.print(time);
      file.print('\t');
      file.print(buttonName);
      file.print('\t');
      file.print("UP");
      file.println();
    }

    if(down) {
      file.print(time);
      file.print('\t');
      file.print(buttonName);
      file.print('\t');
      file.print("DOWN");
      file.println();
    }
}

void notify() {
  if(Ps3.event.button_up.start) {
    isRecording = !isRecording;
    Serial.println(String("Recording ") + (isRecording ? "on" : "off"));

    if(isRecording) {
      filename = String("/rec.txt");
      file = SD.open(filename, "w");
      startRecordingTime = millis();
      file.println("START RECORDING");
    } else {
      file.close();
    }

    return;
  }

  if(Ps3.event.button_up.l1) {
    isAutotriggerOne = !isAutotriggerOne;
    Serial.println(String("Autotrigger one ") + (isAutotriggerOne ? "on" : "off"));
  }

  if(Ps3.event.button_up.r1) {
    isAutotriggerTwo = !isAutotriggerTwo;
    Serial.println(String("Autotrigger two ") + (isAutotriggerTwo ? "on" : "off"));
  }

  if(Ps3.event.button_up.l2) {
    autoTriggerFrequency = max(AUTO_TRIGGER_FREQUENCY_INCREMENT, autoTriggerFrequency - AUTO_TRIGGER_FREQUENCY_INCREMENT);
    Serial.println(String("Decrease autotrigger frequency ") + autoTriggerFrequency);
  }

  if(Ps3.event.button_up.r2) {
    autoTriggerFrequency += AUTO_TRIGGER_FREQUENCY_INCREMENT;
    Serial.println(String("Increase autotrigger frequency ") + autoTriggerFrequency);
  }

  if(isRecording) {
    logInteraction(Ps3.event.button_up.cross || Ps3.event.button_up.square, Ps3.event.button_down.cross || Ps3.event.button_down.square, "ONE");
    logInteraction(Ps3.event.button_up.triangle || Ps3.event.button_up.circle, Ps3.event.button_down.triangle || Ps3.event.button_down.circle, "TWO");

    logInteraction(Ps3.event.button_up.up, Ps3.event.button_down.up, "UP");
    logInteraction(Ps3.event.button_up.right, Ps3.event.button_down.right, "RIGHT");
    logInteraction(Ps3.event.button_up.down, Ps3.event.button_down.down, "DOWN");
    logInteraction(Ps3.event.button_up.left, Ps3.event.button_down.left, "LEFT");
  }
}

/**
 * Initialize OLED Display
 */
void initializeOledDisplay() {
  Serial.print("Initializing OLED display...\t");
  if(!display.begin(SSD1306_SWITCHCAPVCC, OLED_DISPLAY_I2C_ADDRESS)) { 
    halt("ERROR");
  }
  display.clearDisplay();
  display.setTextSize(1);
  display.setTextColor(WHITE);
  //display.setCursor(0, 0);
  //display.display();
  Serial.println("OK");
}

/**
 * Initialize PS3 Controller (bluetooth)
 */
void initializePs3Controller() {
  Serial.print("Initializing PS3 controller...\t");
  Ps3.attach(notify);
  if(!Ps3.begin()) {
    halt("ERROR");
  }
  Serial.println("OK");
}

/**
 * Initialize SD Card
 */
void initializeSdCard() {
  Serial.print("Initializing SD card...\t\t");
  if(!SD.begin(5)) {
    halt("ERROR");
  }
  Serial.println("OK");
}

void draw() {
  display.clearDisplay();
  
  if(!Ps3.isConnected()) {
    display.setCursor(0, 0 * LINE_HEIGHT);
    display.print("DISCONNECTED");
    display.setCursor(0, 2 * LINE_HEIGHT);
    display.print(String("MAC ") + Ps3.getAddress());
    display.display();
    return;
  }

  display.setCursor(0, 0 * LINE_HEIGHT);
  display.print("MANUAL");
  if(isRecording) {
    display.setCursor(0, 2 * LINE_HEIGHT);
    display.print(String("Recording: ") + (isRecording ? filename : "off"));
  }  
  display.setCursor(0, 3 * LINE_HEIGHT);
  display.print(String("Trigger:") + String(autoTriggerFrequency, 1) + "Hz " + (isAutotriggerOne ? " 1" : "") + (isAutotriggerTwo ? " 2" : ""));  
  display.display();
}

void setup() {
  // Initialize default UART over USB
  Serial.begin(9600);

  initializeOledDisplay();
  initializePs3Controller();
  initializeSdCard();
  
  // Initialize output pins
  pinMode(UP_PIN, OUTPUT);
  pinMode(RIGHT_PIN, OUTPUT);
  pinMode(DOWN_PIN, OUTPUT);
  pinMode(LEFT_PIN, OUTPUT);
  pinMode(ONE_PIN, OUTPUT);
  pinMode(TWO_PIN, OUTPUT);
}

void loop() {
  draw();

  digitalWrite(UP_PIN, Ps3.data.button.up);
  digitalWrite(RIGHT_PIN, Ps3.data.button.right);
  digitalWrite(DOWN_PIN, Ps3.data.button.down);
  digitalWrite(LEFT_PIN, Ps3.data.button.left);

  if(isAutotriggerOne || isAutotriggerTwo) {
    uint16_t periodMs = 1000 / autoTriggerFrequency; // period in ms
    uint16_t halfPeriodMs = periodMs / 2;
    uint16_t periodInstant = millis() % periodMs; // a value from 0..period
    digitalWrite(ONE_PIN, isAutotriggerOne && periodInstant < halfPeriodMs);
    digitalWrite(TWO_PIN, isAutotriggerTwo && periodInstant > halfPeriodMs);
  } else {
    digitalWrite(ONE_PIN, Ps3.data.button.cross || Ps3.data.button.square);
    digitalWrite(TWO_PIN, Ps3.data.button.triangle || Ps3.data.button.circle);
  }  
}
