#!/usr/bin/env bash

firmware_filename="ESP32_GENERIC-20231005-v1.21.0.bin"
baud_rate="460800"
device="/dev/ttyUSB0"

esptool_base_command="esptool.py --chip=esp32 --port=${device} --baud=${baud_rate}"

function clean() {
  rm "${firmware_filename}"
}

function clean_exit_error() {
  clean
  printf "\033[31mERROR\033[0m\n"
  exit 1
}

function clean_exit_success() {
  clean
  printf "\033[32mSUCCESS\033[0m\n"
  exit 0
}

function download() {
  url=https://micropython.org/resources/firmware/${firmware_filename}
  printf "Downloading MicroPython '${firmware_filename}' firmware for ESP32 from '${url}'...\n"
  curl --fail --silent "${url}" > ${firmware_filename}
  if [[ $? != "0" ]]; then
    clean_exit_error
  fi
}

function erase() {
  printf "Erasing ESP32 flash on device '${device}' using baud rate '${baud_rate}' (takes about 10s)...\n"
  ${esptool_base_command} erase_flash > /dev/null
  if [[ $? != "0" ]]; then
    clean_exit_error
  fi
}

function flash() {
  printf "Writing MicroPython firmware to ESP32 on device '${device}' using baud rate '${baud_rate}' (takes about 30s)...\n"
   ${esptool_base_command} write_flash --compress 0x1000 ${firmware_filename} > /dev/null
   if [[ $? != "0" ]]; then
    clean_exit_error
  fi
}

download
erase
flash
clean_exit_success
