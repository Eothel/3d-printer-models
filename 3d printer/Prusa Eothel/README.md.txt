# Prusa i3 Eothel

Here you find customized parts for my version of Prusa i3 Mk2s. It is almost identical to the original Prusa i3 Mk2s with only slight mofifications. The original parts I bought of eBay were adapted for using non-prusa limit switches and 5mm threaded rods for the Z-axis. The files here are customizations of the x-ends for 8mm lead screws with brass nut commonly found on AliExpress.

Original Prusa i3 files are found here: https://github.com/prusa3d/Original-Prusa-i3/tree/MK2.

The changes made are:

- Changed hole for z-axis leadscrew brass nut.
- Added counterbore for nut and head of x-end-idler (my originally bought parts had them, but the Prusa SCAD files I found didn't for some reason?).

